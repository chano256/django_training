from django.db import models
from rest_framework.validators import UniqueValidator

# Create your models here.
class User(models.Model):
    first_name = models.CharField(max_length=15)
    last_name = models.CharField(max_length=15)
    email = models.EmailField(max_length=50, unique=True)
    password = models.CharField(max_length=150)

    class Meta:
        ordering = ['first_name']
