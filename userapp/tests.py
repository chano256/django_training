from hashlib import md5
from rest_framework.test import APITestCase
from rest_framework import test
from userapp.models import User
from django.urls import reverse
from rest_framework import status

# Create your tests here.
class UserTest(APITestCase):
    def test_a_user_can_be_created(self):
        data = {
            "first_name":"Allan",
            "last_name": "Ochan",
            "email": "allan@yahoo.com",
        }
        response = self.client.post('/api/users/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().email, 'allan@yahoo.com')