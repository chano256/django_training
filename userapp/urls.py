from django.urls import path
from userapp.views import UserView

urlpatterns = [
    path('users/', UserView.users),
    path('user/<int:pk>/', UserView.user),
]