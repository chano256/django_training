from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from userapp.models import User
from userapp.serializers import UserSerializer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView

# Create your views here.
class UserView(ListAPIView):
    @csrf_exempt
    def users(request):
        if request.method == 'GET':
            users = User.objects.all()
            serializer = UserSerializer(users, many=True)
            return JsonResponse(serializer.data, safe=False)

        if request.method == 'POST':
            data = JSONParser().parse(request)
            serializer = UserSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return JsonResponse(serializer.data, safe=False, status=201)
            return JsonResponse(serializer.errors, status=400)

    @csrf_exempt
    def user(request, pk):
        try:
            user = User.objects.get(pk=pk)
        except User.DoesNotExist:
            return HttpResponse(status=404)

        if request.method == 'GET':
            serializer = UserSerializer(user)
            return JsonResponse(serializer.data)

        if request.method == 'PATCH':
            data = JSONParser().parse(request)
            serializer = UserSerializer(user, data=data)
            if serializer.is_valid():
                serializer.save()
                return JsonResponse(serializer.data)
            return JsonResponse(serializer.errors, status=400)

        if request.method == 'DELETE':
            user.delete()
            return HttpResponse(status=204)