USER MANAGEMENT APP (Django Training)

sudo apt-get install python3-pip <!--Install pip -->
sudo pip3 install virtualenv  <!--Install virtualenv using pip for virtual environments -->
cd django_training
source env/bin/activate <!-- Activate the virtual env-->
pip install -r requirements.txt <!-- install the requirements -->

python manage.py makemigrations && python manage.py migrate <!-- Make & run migrations -->
python manage.py runserver <!-- run project -->
